<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {

    $background = 'home-bg.jpg';
    $sectionTitle  = 'Clean Blog';
    $subtitle = 'A Clean Blog Theme by Start Bootstrap';

    //$pageTitle = 'Clean Blog';

    //return view('welcome')->withBackground($background);
    //return view('welcome')->with(['background'=>$background,'pageTitle'=>$pageTitle,'subtitle'=>$subtitle]);
    return view('welcome')->with(compact('sectionTitle','background','subtitle'));
    //return view('welcome', compact('background','pageTitle','subtitle'));
});

Route::get('/about', function () {

    $background = 'about-bg.jpg';
    $sectionTitle  = 'About Me';
    $subtitle = 'This is what I do!';

    //$pageTitle = 'Clean Blog ';

    return view('Pages.about')->with(compact('sectionTitle','background','subtitle'));
});

Route::get('/contact', function () {

    $background = 'contact-bg.jpg';
    $sectionTitle  = 'Contact Me';
    $subtitle = 'Have questions? I have answers (maybe).';



    return view('Pages.contact')->with(compact('sectionTitle','background','subtitle'));
});

Route::get('/post', 'BlogController@index');
Route::get('/add', 'BlogController@create');
Route::post('/store', 'BlogController@store');


Route::get('/edit', 'BlogController@edit');

