<?php
/**
 * Created by PhpStorm.
 * User: BITM Trainer - 302
 * Date: 5/6/2016
 * Time: 9:48 AM
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;



class BlogController extends Controller
{
    public function  index() {

        $background = 'post-bg.jpg';
        $sectionTitle  = 'Man must explore, and this is exploration at its greatest';
        $subtitle = 'Problems look mighty small from 150 miles up';


        return view('Article.post')->with(compact('sectionTitle','background','subtitle'));
    }

    public function create(){

        return view('Blog.create');
    }

    public function store(Request $request){

       //$fullName = $request::get('fullName');
        $data = $request->all();
        dd($data);
        return view('Blog.store');
    }
}